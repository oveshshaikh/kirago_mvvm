package com.kiraGo.store.di

import android.app.Application
import com.kiraGo.store.GoApplication
import com.kiraGo.store.MainActivity
import com.kiraGo.store.works.AssignedInvWork
import com.kiraGo.store.works.SendFCMWork
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjection
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AndroidInjectionModule::class,
        AppModule::class,
        MainActivityModule::class))
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(goApplication: GoApplication)

    fun injectInWorker(sendFCMWork: SendFCMWork)

    fun injectInAssignedInvWork(assignedInvWork: AssignedInvWork)

}