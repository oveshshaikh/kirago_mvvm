package com.kiraGo.store.di

import com.kiraGo.store.MainActivity
import com.kiraGo.store.ui.Registeration.RegisterActivity

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class MainActivityModule {
    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeRegisterActivity(): RegisterActivity
}
