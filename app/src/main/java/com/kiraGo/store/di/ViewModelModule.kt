package com.kiraGo.store.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kiraGo.store.ui.AssignedInvoices.AssignedInvoicesViewModel
import com.kiraGo.store.ui.InvoiceDetails.InvoiceDetailsViewModel
import com.kiraGo.store.ui.Registeration.RegisterViewModel
import com.kiraGo.store.ui.main.MainViewModel
import com.kiraGo.store.viewmodel.KiraGoViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: MainViewModel): ViewModel
    @Binds
    @IntoMap
    @ViewModelKey(AssignedInvoicesViewModel::class)
    abstract fun bindAssignedViewModel(assignedInvoicesViewModel: AssignedInvoicesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(InvoiceDetailsViewModel::class)
    abstract fun bindInvDetailsViewModel(invoiceDetailViewModel: InvoiceDetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegisterViewModel::class)
    abstract fun bindRegistrationViewModel(registerViewModel: RegisterViewModel): ViewModel
  /*  @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    abstract fun bindSearchViewModel(searchViewModel: SearchViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RepoViewModel::class)
    abstract fun bindRepoViewModel(repoViewModel: RepoViewModel): ViewModel
*/
    @Binds
    abstract fun bindViewModelFactory(factory: KiraGoViewModelFactory): ViewModelProvider.Factory
}