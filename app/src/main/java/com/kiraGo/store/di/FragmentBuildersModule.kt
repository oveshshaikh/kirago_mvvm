package com.kiraGo.store.di


import com.kiraGo.store.ui.AssignedInvoices.AssignedInvoicesFragment
import com.kiraGo.store.ui.InvoiceDetails.InvoiceDetails
import com.kiraGo.store.ui.main.MainFragment

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeMainFragment(): MainFragment

    @ContributesAndroidInjector
    abstract fun contributeAssignedInvFragment(): AssignedInvoicesFragment

    @ContributesAndroidInjector
    abstract fun contributeInvoiceDetailsFragment(): InvoiceDetails
}
