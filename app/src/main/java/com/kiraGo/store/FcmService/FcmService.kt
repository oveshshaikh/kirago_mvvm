package com.kiraGo.store.FcmService

import android.util.Log
import androidx.work.*
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.kiraGo.store.utils.AppConstants
import com.kiraGo.store.works.AssignedInvWork
import com.kiraGo.store.works.SendFCMWork

class FcmService : FirebaseMessagingService() {

    val TAG = "FcmService"
    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        Log.d(TAG,"received msg ")
        if (remoteMessage == null)
            return
        else {
            val map = remoteMessage.data
            for (key in map.keys) {
                Log.e(key, map[key])
            }
            if (map.containsKey("tag")) {
                when (map["tag"]) {
                    "AssignGoUser" -> {
                        val data = map["user_data"]
                        if (data != null)
                            scheduleAssignINVWork(data)
                    }
                }
            }
        }
    }

    private fun scheduleAssignINVWork(data: String?) {
        val constraint = Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED)
                .build()
        val onetimereq = OneTimeWorkRequest.Builder(AssignedInvWork::class.java).setConstraints(constraint)
                .setInputData(createInputData(data))
                .build()

        WorkManager.getInstance().enqueue(onetimereq)
    }


    fun createInputData(data: String?): Data {
        return Data.Builder()
                .putString("data", data)
                .build()
    }
}