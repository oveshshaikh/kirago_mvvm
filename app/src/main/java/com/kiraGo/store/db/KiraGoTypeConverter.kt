package com.kiraGo.store.db

import androidx.room.TypeConverter
import java.util.*



object KiraGoTypeConverter {
    @TypeConverter
    @JvmStatic
    fun toDate(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }

    @TypeConverter
    @JvmStatic
    fun toLong(value: Date?): Long? {
        return value?.time!!.toLong()
    }

}