package com.kiraGo.store.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [OrderTable::class, OrderDetails::class, ProductTable::class,CustomerTable::class], version = 1, exportSchema = false)
@TypeConverters(KiraGoTypeConverter::class)
abstract class KiraGoDB : RoomDatabase() {
    abstract fun assignedInvDAO(): AssignedInvDAO
}