package com.kiraGo.store.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class CustomerTable(@PrimaryKey
                         @SerializedName("cust_id")
                         var cust_id: Long = 0,
                         @SerializedName("c_name")
                         var c_name: String = String(),
                         @SerializedName("c_mobile_no")
                         var c_mobile_no: String = String(),
                         @SerializedName("c_alt_mobile_no")
                         var c_alt_mobile_no: String = String(),
                         @SerializedName("c_address")
                         var c_address: String = String(),
                         @SerializedName("c_alter_addr")
                         var c_alter_addr: String = String())