package com.kiraGo.store.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class ProductTable(@PrimaryKey
                   @SerializedName("id")
                   var id: Long = 0,
                        @SerializedName("barcode")
                   val barcode: String = String(),
                        @SerializedName("product_name")
                   val product_name: String = String(),
                        @SerializedName("product_image")
                   val product_image: String = String(),
                        @SerializedName("brand")
                   val brand: String = String(),
                        @SerializedName("category_id")
                   val category_id: String = String(),
                        @SerializedName("unit_size")
                   val unit_size: String = String(),
                        @SerializedName("unit_price")
                   val unit_price: String = String(),
                        @SerializedName("unit_weight")
                   val unit_weight: String = String(),
                        @SerializedName("units_in_stock")
                   val units_in_stock: String = String(),
                        @SerializedName("parent_unit_id")
                   val parent_unit_id: String = String(),
                        @SerializedName("quantity_per_unit")
                   val quantity_per_unit: String = String())