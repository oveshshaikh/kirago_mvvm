package com.kiraGo.store.ui.main

import android.content.Intent
import android.graphics.Color
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.TypedValue
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.work.*
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.iid.FirebaseInstanceId
import com.kiraGo.store.Adapters.CommomTabAdp
import com.kiraGo.store.R
import com.kiraGo.store.di.Injectable
import com.kiraGo.store.ui.AssignedInvoices.AssignedInvoicesFragment
import com.kiraGo.store.ui.AssignedInvoices.AssignedInvoicesViewModel
import com.kiraGo.store.utils.App
import com.kiraGo.store.utils.AppConstants
import com.kiraGo.store.utils.AppConstants.FCM_TOKEN
import com.kiraGo.store.works.SendFCMWork
import kotlinx.android.synthetic.main.main_fragment.*
import javax.inject.Inject
import android.os.Build
import android.view.MenuInflater
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import com.kiraGo.store.GoApplication
import com.kiraGo.store.GoApplication_MembersInjector
import com.kiraGo.store.ui.Registeration.RegisterActivity


class MainFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    companion object {
        fun newInstance() = MainFragment()
    }

    @Inject
    lateinit var app: App

    @Inject
    lateinit var goApp: GoApplication

    private lateinit var toolbar: Toolbar
    private lateinit var viewModel: AssignedInvoicesViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    lateinit var rootview: View
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rootview = view
        toolbar = rootview.findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            (activity as AppCompatActivity).setSupportActionBar(toolbar)
        }
        setHasOptionsMenu(true);
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = activity?.run { ViewModelProviders.of(this, viewModelFactory).get(AssignedInvoicesViewModel::class.java) }
                ?: throw Exception("Invalid Activity")
        val adapter = CommomTabAdp(activity!!.supportFragmentManager)
        adapter.addFrag(listOf(AssignedInvoicesFragment.newInstance(0), AssignedInvoicesFragment.newInstance(2), AssignedInvoicesFragment.newInstance(3)),
                listOf("Orders", "Delivered", "Absent"))
        viewpager.adapter = adapter
        tabs.setupWithViewPager(viewpager)


        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener { task ->
            if (!task.isSuccessful) {
                return@addOnCompleteListener
            }
            val fcmtoken = task.result?.token
            if (app.getTok(FCM_TOKEN) != fcmtoken)
                scheduleFCMWork(fcmtoken)
        }
        if (AppConstants.inNetwork(context!!)) {
            viewModel.getInvoicesfromWeb()
        } else {
            viewModel.getInvoicesfromWeb()
            unableRetry(rootview);
        }

        viewModel.isLoggingOut.observe(this, Observer {
            if(it) {
                activity?.finish()
                this.startActivity(Intent(activity, RegisterActivity::class.java))
            } else {
                Toast.makeText(rootview.context, "Something went wrong", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun scheduleFCMWork(fcmtoken: String?) {
        val constraint = Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED)
                .build()
        val onetimereq = OneTimeWorkRequest.Builder(SendFCMWork::class.java).setConstraints(constraint)
                .setInputData(createInputData(fcmtoken))
                .build()

        WorkManager.getInstance().enqueue(onetimereq)
    }


    fun createInputData(fcmtoken: String?): Data {
        return Data.Builder()
                .putString(FCM_TOKEN, fcmtoken)
                .build()
    }

    fun unableRetry(v: View) {
        val bar = Snackbar.make(v, getString(R.string.no_internet), BaseTransientBottomBar.LENGTH_INDEFINITE)
        val snackView = bar.view
        val tv = snackView.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, v.context.resources.getDimension(R.dimen.text_size_medium))
        snackView.setBackgroundColor(ContextCompat.getColor(v.context, R.color.error_red))
        bar.setActionTextColor(Color.WHITE)
        bar.setAction("Retry") {
            if (AppConstants.inNetwork(context!!)) {
                bar.dismiss()
                viewModel.getInvoicesfromWeb()
            } else {
                unableRetry(rootview)
            }
        }
        bar.show()

    }

    /* fun onCreateOptionsMenu(menu: Menu): Boolean {
         // Inflate the menu; this adds items to the action bar if it is present.
         getMenuInflater().inflate(R.menu.main_menu, menu)
         return true
     }*/
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.logout_btn -> {
                if(goApp.inNetwork()) {
                    viewModel.logout()
                } else {
                    Toast.makeText(context, "Internet Required!", Toast.LENGTH_SHORT).show()
                }
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


}
