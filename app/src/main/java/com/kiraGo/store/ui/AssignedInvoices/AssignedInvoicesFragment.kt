package com.kiraGo.store.ui.AssignedInvoices

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.kiraGo.store.Adapters.InvicesAdapter

import com.kiraGo.store.R
import com.kiraGo.store.di.Injectable
import com.kiraGo.store.ui.InvoiceDetails.InvoiceDetails
import com.kiraGo.store.utils.AppConstants
import com.kiraGo.store.utils.AppConstants.ORDER_STATUS
import com.kiraGo.store.utils.EmptyInvoicesException
import com.kiraGo.store.utils.InvoiceClickListner
import com.kiraGo.store.vo.InvoiceModel
import com.kiraGo.store.works.SendFCMWork
import kotlinx.android.synthetic.main.assigned_invoices_fragment.*
import javax.inject.Inject

class AssignedInvoicesFragment : Fragment(), Injectable, InvoiceClickListner, SwipeRefreshLayout.OnRefreshListener {
    override fun onRefresh() {
        if (AppConstants.inNetwork(context!!)) {
            swipe_refresh_layout.isRefreshing = true
            viewModel.getInvoicesfromWeb()
        } else {
            swipe_refresh_layout.isRefreshing = false
            Toast.makeText(context, R.string.net_required, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onInvoiceSelected(orderId: Long) {
        Log.d(TAG, "order id : " + orderId)
        val fragmentManager = activity!!.supportFragmentManager
        fragmentManager.beginTransaction()
                //  .remove(fragmentManager.findFragmentById(R.id.container))
                .add(R.id.container, InvoiceDetails.newInstance(orderId))
                .addToBackStack(null)
                .commit()
        /*val intent = Intent(activity, InvoceDetailActivity::class.java)
        startActivity(intent)*/
    }

    val TAG = "AssignedFragment"
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    var assigneStatus: Int = 1
    lateinit var adapter: InvicesAdapter

    companion object {
        fun newInstance(status: Int): AssignedInvoicesFragment {
            val fragment = AssignedInvoicesFragment()
            val bundle = Bundle(1)
            bundle.putInt(ORDER_STATUS, status)
            fragment.arguments = bundle
            return fragment
        }
    }

    private lateinit var viewModel: AssignedInvoicesViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootview = inflater.inflate(R.layout.assigned_invoices_fragment, container, false)
        return rootview
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        assigneStatus = arguments?.getInt(ORDER_STATUS, 0) ?: 0

        swipe_refresh_layout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent)
        swipe_refresh_layout.setOnRefreshListener(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = activity?.run { ViewModelProviders.of(this, viewModelFactory).get(AssignedInvoicesViewModel::class.java) } ?: throw Exception("Invalid Activity")
        viewModel.subsribetoLiveInvoicesForStatus(assigneStatus)
        adapter = InvicesAdapter(context!!, this)
        recycler_view.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        recycler_view.adapter = adapter
        viewModel.getInvoicesLiveData()?.observe(this, Observer { orders ->
            if (orders != null) {
                Log.d(TAG, "invoice_added")
                Log.d(TAG,orders.size.toString());
                if (orders.size > 0) {
                    adapter.submitList(orders)
                    adapter.notifyDataSetChanged()
                } else {
                    emptylayout.visibility = View.VISIBLE
                    progressbar.visibility = View.GONE
                    recycler_view.visibility = View.GONE
                }
            }
        })
        viewModel.isLoading.observe(this, Observer<Boolean> {
            it?.let { showLoadingDialog(it) }
        })
        viewModel.apiError.observe(this, Observer<Throwable> {
            it?.let {
                Log.d(TAG, "APi error" + it.message)
                if (it != null) {
                    if (it is EmptyInvoicesException) {
                        //  Toast.makeText(context, getString(R.string.oops) + "\n" + getString(R.string.offline_data_msg), Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(context, getString(R.string.oops), Toast.LENGTH_SHORT).show()
                    }
                }
            }
            //Snackbar.make(context, it.localizedMessage, Snackbar.LENGTH_LONG).show() }
        })
        viewModel.getliveInvoicelist(assigneStatus).observe(this, Observer {

            it.forEach { Log.d(TAG, "liveobersver_:"+it.c_name) }

        })

    }

    private fun showLoadingDialog(show: Boolean) {
        if (show) {
            progressbar.visibility = View.VISIBLE
            recycler_view.visibility = View.GONE
            emptylayout.visibility = View.GONE
        } else {
            swipe_refresh_layout.isRefreshing = false
            progressbar.visibility = View.GONE
            emptylayout.visibility = View.GONE
            recycler_view.visibility = View.VISIBLE
            //viewModel.getInvoicesfromWeb()
        }
    }
}
