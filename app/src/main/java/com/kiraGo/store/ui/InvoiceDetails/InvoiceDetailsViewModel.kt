package com.kiraGo.store.ui.InvoiceDetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel;
import com.kiraGo.store.Repos.MainRepository
import com.kiraGo.store.db.OrderDetails
import com.kiraGo.store.db.ProductTable
import com.kiraGo.store.vo.InvoiceModel
import com.kiraGo.store.vo.UpdateStat
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

class InvoiceDetailsViewModel @Inject constructor(val repository: MainRepository) : ViewModel() {

    var isLoading = MutableLiveData<Boolean>()

    var apiError = MutableLiveData<Throwable>()

    var updateStat=MutableLiveData<UpdateStat>()

    fun changeorderStatusLocally(order_id: Long, image: String) {
        repository.changeOrderstatus(order_id, image)
    }

    fun getSingleOrderDetail(order_id: Long): LiveData<InvoiceModel> {
        return repository.getSingleOrderDetails(order_id)
    }


    fun getProductsforOrder(order_id: Long): LiveData<List<ProductTable>> {
        return repository.getProductListforOrder(order_id)
    }

    fun getOrderListforOrder(order_id: Long): LiveData<List<OrderDetails>> {
        return repository.getOrderListforOrder(order_id)
    }

    fun updateOrderStatus(access_token: String, invoiceId: Long, invoiceNo: RequestBody, isComplete: Boolean, signature: MultipartBody.Part) {
        repository.updateStatusofOrder(access_token, invoiceId, invoiceNo, isComplete, signature, {
            updateStat.value=it
            isLoading.value=false

        }, {
            isLoading.value=false
            apiError.value=it
        })
    }
}
