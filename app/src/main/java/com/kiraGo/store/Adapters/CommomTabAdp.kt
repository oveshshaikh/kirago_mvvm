package com.kiraGo.store.Adapters

class CommomTabAdp(mgr: androidx.fragment.app.FragmentManager) : androidx.fragment.app.FragmentPagerAdapter(mgr) {

    private var mFragmentList: List<androidx.fragment.app.Fragment> = emptyList()
    private var mFragmentTitleList: List<String> = emptyList()

    override fun getItem(position: Int) = mFragmentList[position]

    override fun getCount() = mFragmentTitleList.size

    override fun getPageTitle(position: Int) = mFragmentTitleList[position]

    fun addFrag(mFragmentList: List<androidx.fragment.app.Fragment>, mFragmentTitleList: List<String>) {
        this.mFragmentList = mFragmentList
        this.mFragmentTitleList = mFragmentTitleList
    }
}