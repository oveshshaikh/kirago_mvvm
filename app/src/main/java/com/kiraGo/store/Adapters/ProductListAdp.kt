package com.kiraGo.store.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.kiraGo.store.R
import com.kiraGo.store.db.OrderDetails
import com.kiraGo.store.db.ProductTable

class ProductListAdp(var producltList: List<OrderDetails>) : RecyclerView.Adapter<ProductListAdp.ProductListHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductListHolder {
        val view = ProductListHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.product_item, parent, false))
        return view
    }

    override fun onBindViewHolder(holder: ProductListHolder, position: Int) {
        holder.productName.text = producltList.get(position).product_name
        holder.productQty.text = producltList.get(position).quantity
        holder.productRate.text = producltList.get(position).priceToConsider
        holder.productAmt.text = producltList.get(position).mrp

        //holder.productQty.text = "2.0"
    }

    override fun getItemCount(): Int {
        return producltList.size
    }

    class ProductListHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val productName: TextView = itemView.findViewById(R.id.productName)
        val productRate : TextView= itemView.findViewById(R.id.rate)
        val productQty : TextView = itemView.findViewById(R.id.qty)
        val productAmt : TextView = itemView.findViewById(R.id.amt)
    }

    fun addProducts(listProducts: List<OrderDetails>) {
        this.producltList = listProducts
        notifyDataSetChanged()
    }


}