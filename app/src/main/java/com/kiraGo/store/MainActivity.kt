package com.kiraGo.store

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.kiraGo.store.api.ApiCalls
import com.kiraGo.store.di.Injectable
import com.kiraGo.store.ui.Registeration.RegisterActivity
import com.kiraGo.store.ui.main.MainFragment
import com.kiraGo.store.utils.App
import com.kiraGo.store.utils.AppConstants.USER
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasFragmentInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {

        return dispatchingAndroidInjector
    }

    /* @Inject
     lateinit var apiCalls: ApiCalls*/

    @Inject
    lateinit var app: App

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        //  apiCalls.getAllInvoices("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6Ijk5IiwibG9nZ2VkX2luIjp0cnVlLCJkZXZpY2VfaWQiOiJ0anI2RiIsInRpbWUiOjE1NDQ1OTUwODV9.lf2_MjBvIHsXJb-E1_cQ6ZUms3MThU6KGv04X03flcw").execute()

        if (!app.hasKey(USER)) {
            val intent = Intent(this, RegisterActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        } else {
            if (savedInstanceState == null) {
                supportFragmentManager.beginTransaction()
                        .add(R.id.container, MainFragment.newInstance())
                        .commitNow()
            }
        }
    }

}
