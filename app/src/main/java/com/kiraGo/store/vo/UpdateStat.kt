package com.kiraGo.store.vo

import com.google.gson.annotations.SerializedName

data class UpdateStat(@SerializedName("status")
                      val status: Boolean = false,
                      @SerializedName("message")
                      val message: String = String(),
                      @SerializedName("image")
                      val image: String = String())