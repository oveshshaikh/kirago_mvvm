package com.kiraGo.store.vo

import com.google.gson.annotations.SerializedName
import com.kiraGo.store.db.CustomerTable
import com.kiraGo.store.db.OrderDetails
import com.kiraGo.store.db.OrderTable
import com.kiraGo.store.db.ProductTable

data class AssignData(@SerializedName("order")
                      val order: OrderTable,
                      @SerializedName("customer_details")
                      val customer: CustomerTable,
                      @SerializedName("order_details")
                      val orderDetails: List<OrderDetails>,
                      @SerializedName("product")
                      val productz: List<ProductTable>)