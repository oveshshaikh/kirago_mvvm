package com.kiraGo.store.vo

import com.google.gson.annotations.SerializedName

data class RefereshTok (
        @SerializedName("access_token")
        val access_token: String,
        @SerializedName("refresh_token")
        val refresh_token: String,
        @SerializedName("status")
        val status: Boolean
)