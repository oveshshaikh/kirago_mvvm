package com.kiraGo.store.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import androidx.core.content.ContextCompat.getSystemService
import javax.inject.Inject

object AppConstants {
    var BASE_URL = "https://kira.store/kira/api/"
    var REFRESH_TOK_URL = "https://kira.store/kira/api/go_users/refresh"

    val ORDER_STATUS = "order_status"
    val TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEwMSIsImRldmljZV9pZCI6Ik0yR1N4IiwibG9nZ2VkX2luIjp0cnVlLCJ0aW1lIjoxNTQ1MjA1Mjg4fQ.eKPX3-RzysngNIwjT17sdbSltEl6Ugfb7_C7z-5Ul34"
    val ORDER_ID = "order_id"
    var SIGNATURE_URL = "https://kira.store/kira/uploads/deliver_signature/"
    const val KIRA_GO_PREFS = "kiragoprefs"
    const val USER = "user"
    const val FCM_TOKEN = "fcm_token"


    fun inNetwork(context: Context): Boolean {
        var isConnected = false
        val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val nInfo = manager.activeNetworkInfo
        if (nInfo != null && nInfo.isConnected)
            isConnected = true
        return isConnected
    }


}