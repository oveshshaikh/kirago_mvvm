package com.kiraGo.store.utils

class EmptyInvoicesException(errorMessage: String) : Exception(errorMessage)
