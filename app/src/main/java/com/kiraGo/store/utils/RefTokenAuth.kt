package com.kiraGo.store.utils

import android.app.Application
import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat.startActivity
import android.util.Log
import com.kiraGo.store.GoApplication
import com.kiraGo.store.MainActivity
import com.kiraGo.store.api.ApiCalls
import com.kiraGo.store.ui.Registeration.RegisterActivity
import com.kiraGo.store.utils.AppConstants.REFRESH_TOK_URL
import com.kiraGo.store.utils.AppConstants.USER
import com.kiraGo.store.vo.RefereshTok

import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import java.net.HttpURLConnection
import javax.inject.Inject
import okhttp3.OkHttpClient
import com.google.gson.JsonObject
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.kiraGo.store.R
import okhttp3.RequestBody




class RefTokenAuth @Inject constructor(val app: App, val context: Application) : Authenticator {


    /*
          @Inject
          lateinit var app: App

          @Inject
          lateinit var context: GoApplication*/


    override fun authenticate(route: Route?, response: Response?): Request? {
        //  val app = GoController.instance
        if (response?.code() == 401) {

                try {
                    val user = app.getUserD()
                    val tok = user.refresh_token

                    if (!tok.isEmpty()) {

                        val reqbody = RequestBody.create(null, ByteArray(0))

                    val request = Request.Builder()
                            .header("Refresh", tok)
                            .url(REFRESH_TOK_URL)
                            .post(reqbody)
                            .build()

                    val responseval = OkHttpClient().newCall(request).execute()
                    val gson = GsonBuilder().create()
                    val resp = gson.fromJson(responseval.body()!!.string(), RefereshTok::class.java)
                    if (responseval?.code() == HttpURLConnection.HTTP_OK) {
                        user.access_token = resp!!.access_token
                        user.refresh_token = resp!!.refresh_token
                        app.setUserD(user)
                        return response.request()
                                .newBuilder()
                                .removeHeader("Token")
                                .addHeader("Token", user.refresh_token)
                                .build()
                    } else if (responseval?.code() == 406) {
                        app.remove(USER)
                        /* try {
                             JobManager.instance().cancelAll()
                         } catch (a: Exception) {
                             Log.e("JobManager", "cancelAll", a)
                         }*/
                        context.startActivity(Intent(context, RegisterActivity::class.java)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
                    }
                }
            }
            catch (e: Exception)
            {
             Log.e("Exc", "error", e)
                return null
            }
        }
        return null
    }

}