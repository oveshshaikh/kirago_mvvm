package com.kiraGo.store.works

import android.content.Context
import android.util.Log
import androidx.work.Operation
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.firebase.iid.FirebaseInstanceId
import com.kiraGo.store.GoApplication
import com.kiraGo.store.Repos.MainRepository
import com.kiraGo.store.api.ApiCalls
import com.kiraGo.store.di.DaggerAppComponent
import com.kiraGo.store.utils.App
import com.kiraGo.store.utils.AppConstants.FCM_TOKEN
import com.kiraGo.store.utils.AppConstants.USER
import com.kiraGo.store.vo.InvoiceModel
import org.jetbrains.anko.doAsync
import java.io.IOException
import javax.inject.Inject
import javax.security.auth.callback.Callback

class SendFCMWork(context: Context, params: WorkerParameters)
    : Worker(context, params) {

    val TAG = "SendFCMWork"

    @Inject
    lateinit var api: ApiCalls

    @Inject
    lateinit var app: App


    override fun doWork(): Result {
        if (applicationContext is GoApplication) {
            var daggerAppComponent = DaggerAppComponent.builder().application(applicationContext as GoApplication).build()
            daggerAppComponent.injectInWorker(this)
        }

        if (!app.hasKey(USER))
            return Result.success()


        var token = inputData.getString(FCM_TOKEN)

        if (token!!.isEmpty()) {
            token = app.getTok(FCM_TOKEN)
            if (token.isEmpty()) {
                token = FirebaseInstanceId.getInstance().instanceId.getResult()!!.token
                if (token.isEmpty())
                    return Result.success()
            }
        }
        val cal = api.sendFCM("", 1, token, app.getUserD().shopId, app.getUserD().user_id.toString())
        return try {
            val resp = cal.execute()
            if (resp.code() == 200) {
                app.put(FCM_TOKEN, token, false)
                Result.success()
            } else
                Result.retry()
        } catch (e: IOException) {
            Log.e(TAG, "key", e)
            Result.retry()
        }
    }
}