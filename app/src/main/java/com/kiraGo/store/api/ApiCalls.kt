package com.kiraGo.store.api

import com.kiraGo.store.vo.RefereshTok
import com.kiraGo.store.vo.RegResp
import com.kiraGo.store.vo.UpdateStat
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiCalls {
    @FormUrlEncoded
    @POST("go_users/verify_go_user")
    fun reg(@Field("verification_code") code: String = String()): Call<RegResp>

    @FormUrlEncoded
    @POST("offline_data_syn/manage_fcm_key")
    fun sendFCM(@Header("Token") access_token: String = String(),
                @Field("app_id") appId: Int = 1,
                @Field("fc_key") fc_key: String = String(),
                @Field("store_id") store_id: String = String(),
                @Field("go_id") go_id: String = String()): Call<Void>

    @FormUrlEncoded
    @POST("go_users/get_assign_order")
    fun getInvoices(@Header("Token") access_token: String = String(),
                    @Field("order") data: String = String()): Call<GetInvResp>


    @POST("go_users/get_go_user_orders")
    fun getAllInvoices(@Header("Token") access_token: String = String()): Call<GetInvResp>

    @Multipart
    @POST("go_users/update_delivery_status")
    fun updateStatus(@Header("Token") access_token: String = String(),
                     @Part("invoice_id") invoiceId: RequestBody,
                     @Part("invoice_no") invoiceNo: RequestBody,
                     @Part("isComplete") isComplete: RequestBody,
                     @Part signature: MultipartBody.Part): Call<UpdateStat>
    @POST("go_users/refresh")
    fun referesh(@Header("Refresh") refresh: String): Call<RefereshTok>

    @FormUrlEncoded
    @POST("go_users/logout_gouser")
    fun logout(@Header("Token") access_token: String = String(),
               @Field("user_id") user_id: Long=1):Call<RegResp>
}